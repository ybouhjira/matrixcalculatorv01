#include <QApplication>
#include <QMdiSubWindow>
#include <QMessageBox>
#include <QWidget>
#include <QInputDialog>
#include <QStringList>
#include <QFileDialog>
#include "FenetrePrincipale.h"
#include "calcul.h"
#include "matrice.h"
#include <cstdio>
#include <cctype>

FenetrePrincipale::FenetrePrincipale(QWidget *parent) : QMainWindow(parent)
{
    welcome = true;
    //ecran de bienvenue:
    labelBienvenue = new QLabel;
    imagePng = new QPixmap(":/img/res/welcome600.png");
    //barre de menu:
    setMenuBar(new QMenuBar);
    //menus:
    menuMatrice = menuBar()->addMenu("Matrice");
    menuCalcul = menuBar()->addMenu("Calcul");
    menuAide = menuBar()->addMenu("Aide");
    tabWidget = new QTabWidget;

    //Volet (Dock):
    volet = new QDockWidget("Que voulez vous calculez ?",this);

    //Composent du volet:
        /*layout*/
    layout = new QHBoxLayout;
        /*boutons*/
    boutonAdd = new QPushButton("Somme");
    boutonSstr = new QPushButton("Soustraction");
    boutonPrd = new QPushButton("Produit");
    boutonT = new QPushButton("Transpose");
    boutonTr = new QPushButton("Trace");
    boutonCom = new QPushButton("Comatrice");
    boutonInv = new QPushButton("Inverse");
    boutonDet = new QPushButton("Determinant");
        /*Sroll Area*/
    scrollArea = new QScrollArea;
        /*widget de base*/
    zoneBoutons = new QWidget;

    Setup();
    Connections();
}

inline void FenetrePrincipale::Setup()
{
    menuCalcul->setEnabled(false);
    setWindowTitle("Matrix Calculator");
    //mettre les menus:
    menuAide->addAction(QIcon(":/ico/res/help.png"),"A propos",&about,SLOT(show()));
    menuAide->addAction(QIcon(":/ico/res/qt.png"),"A propos de Qt",qApp,SLOT(aboutQt()));
    menuMatrice->addAction(QIcon(":/ico/res/add.png"),"Ajouter matrice",&dialog,SLOT(show()));
    menuMatrice->addAction(QIcon(":/ico/res/remove.png"),"Supprimer",this,SLOT(SuprrMat()));
    menuMatrice->addAction(QIcon(":/ico/res/rename.png"),"Renommer");
    menuMatrice->addSeparator();
    menuMatrice->addAction(QIcon(":/ico/res/open.png"),"Ouvrir");
    menuMatrice->addAction(QIcon(":/ico/res/save.png"),"Enregistrer");
    menuMatrice->addSeparator();
    menuMatrice->addAction("Quitter",qApp,SLOT(quit()));
    menuCalcul->addAction("Addition");
    menuCalcul->addAction("Soustraction");
    menuCalcul->addAction("Produit");
    menuCalcul->addAction("Transpose");
    menuCalcul->addAction("Trace");
    menuCalcul->addAction("Comatrice");
    menuCalcul->addAction("Inverse");
    menuCalcul->addAction("Determinant");
    //mettre la barre d'outils
    toolBarMat = addToolBar("Matrices");
    toolBarMat->setIconSize(QSize(40,40));
    toolBarMat->setMovable(false);
    toolBarMat->setToolButtonStyle(Qt::ToolButtonTextBesideIcon	);
    toolBarMat->addAction(menuMatrice->actions()[0]);
    toolBarMat->addAction(menuMatrice->actions()[1]);
    toolBarMat->addAction(menuMatrice->actions()[2]);
    toolBarMat->addAction(menuMatrice->actions()[3]);
    toolBarMat->addAction(menuMatrice->actions()[4]);
    toolBarMat->addAction(menuMatrice->actions()[5]);

    //mettre la zone MDI (centrale)
    setCentralWidget(tabWidget);
    //mdiArea->setViewMode(QMdiArea::TabbedView); //mode onglet

    //Volet de control:
    addDockWidget(Qt::BottomDockWidgetArea,volet);
    volet->setWidget(zoneBoutons);
    zoneBoutons->setLayout(layout);
    zoneBoutons->setLayout(layout);
    layout->addWidget(boutonAdd);
    layout->addWidget(boutonSstr);
    layout->addWidget(boutonPrd);
    layout->addWidget(boutonT);
    layout->addWidget(boutonTr);
    layout->addWidget(boutonCom);
    layout->addWidget(boutonCom);
    layout->addWidget(boutonInv);
    layout->addWidget(boutonDet);

    boutonAdd->setMinimumHeight(30);
    boutonSstr->setMinimumHeight(30);
    boutonPrd->setMinimumHeight(30);
    boutonT->setMinimumHeight(30);
    boutonTr->setMinimumHeight(30);
    boutonDet->setMinimumHeight(30);
    boutonInv->setMinimumHeight(30);
    boutonCom->setMinimumHeight(30);
    boutonAdd->setMaximumHeight(600);
    boutonSstr->setMaximumHeight(600);
    boutonPrd->setMaximumHeight(600);
    boutonT->setMaximumHeight(600);
    boutonTr->setMaximumHeight(600);
    boutonDet->setMaximumHeight(600);
    boutonInv->setMaximumHeight(600);
    boutonCom->setMaximumHeight(600);
    dialog.setWindowModality(Qt::ApplicationModal);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setFixedWidth(140);
    connect(&dialog, SIGNAL(pushedOk()), this, SLOT(AjouterMat()) );
    volet->setFeatures(QDockWidget::NoDockWidgetFeatures);
    volet->setFixedHeight(50);
    volet->setTitleBarWidget(new QWidget);
    volet->titleBarWidget()->hide();

    //ecran de bienvenue
    volet->setDisabled(true);
    menuMatrice->actions()[1]->setDisabled(true);
    menuMatrice->actions()[2]->setDisabled(true);
    menuMatrice->actions()[5]->setDisabled(true);

    QLabel *label = new QLabel(labelBienvenue);
    label->setPixmap(*imagePng);
    setFixedSize(800,600);
    label->setGeometry(100,25,600,396);
    tabWidget->addTab(labelBienvenue,"Matrix calculator - Bienvenue");
}

void FenetrePrincipale::Connections()
{
    connect(boutonDet, SIGNAL(clicked()) ,this ,SLOT(CalcDet()));
    connect(menuCalcul->actions()[7], SIGNAL(triggered()), this, SLOT(CalcDet()));
    connect(boutonTr, SIGNAL(clicked()), this, SLOT(CalcTr()));
    connect(menuCalcul->actions()[4], SIGNAL(triggered()), this, SLOT(CalcTr()) );
    connect(tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabWidgetVide()) );
    connect(boutonT, SIGNAL(clicked()), this, SLOT(CalcT()));
    connect(menuCalcul->actions()[3], SIGNAL(triggered()), this, SLOT(CalcT()));
    connect(boutonCom, SIGNAL(clicked()), this, SLOT(CalcCom()) );
    connect(menuCalcul->actions()[5], SIGNAL(triggered()), this, SLOT(CalcCom()));
    connect(boutonInv, SIGNAL(clicked()), this, SLOT(CalcInvers()));
    connect(menuCalcul->actions()[6], SIGNAL(triggered()), this, SLOT(CalcInvers()) );
    connect(menuMatrice->actions()[2], SIGNAL(triggered()), this, SLOT(RenomMat()) );
    connect(boutonAdd, SIGNAL(clicked()), this, SLOT(CalcAdd()) );
    connect(menuCalcul->actions()[0], SIGNAL(triggered()), this, SLOT(CalcAdd()) );
    connect(boutonSstr, SIGNAL(clicked()), this, SLOT(CalcSstr()));
    connect(menuCalcul->actions()[1], SIGNAL(triggered()), this ,SLOT(CalcSstr()));
    connect(boutonPrd, SIGNAL(clicked()), this, SLOT(CalcPrd()) );
    connect(menuCalcul->actions()[2], SIGNAL(triggered()), this, SLOT(CalcPrd()) );
    connect(menuMatrice->actions()[5], SIGNAL(triggered()), this, SLOT(Enregistrer()) );
    connect(menuMatrice->actions()[4], SIGNAL(triggered()), this, SLOT(Ouvrir()));
}

void FenetrePrincipale::AjouterMat()
{

    if(welcome){
        tabWidget->clear();
        welcome = false;
    }

    CentreFenetre *newMatInput = new CentreFenetre(dialog.getL(),dialog.getC());
    lsWCentral << newMatInput ;

    //nomm de la matrice
    QString inputNom = dialog.getNom();
    for(int i=0; i<lsWCentral.size(); i++)
        if(lsWCentral[i]->getNom() == inputNom){ //si le nom existe deja
            i=0;
            inputNom += "*";
        }

    //mettre le nom:
    newMatInput->setNom(inputNom);
    tabWidget->addTab(newMatInput,inputNom);
}

void FenetrePrincipale::SuprrMat()
{
    if(tabWidget->count() != 0){
        int index = tabWidget->currentIndex();
        tabWidget->removeTab(index);
        lsWCentral.removeAt(index);
    }
}

void FenetrePrincipale::CalcDet()
{
    if(tabWidget->count() != 0){
        int index = tabWidget->currentIndex();
        matrice curMat = lsWCentral[index]->getMatrice();

        if(curMat.l!=curMat.c)
            QMessageBox::critical(this,"Impossible de calculer le determinant","Cette matrice n'est pas carre");
        else if(curMat.c == 1)
            QMessageBox::information(this,"Determinant","Determinant: "+QString::number(curMat.mat[0][0]));
        else
            QMessageBox::information(
                this,"Determinant",
                "Detereminant: " +
                QString::number(Det(lsWCentral[index]->getMatrice()))
                );
    }
}

void FenetrePrincipale::CalcTr()
{
    if(tabWidget->count() != 0){
        int index = tabWidget->currentIndex();
        matrice curMat = lsWCentral[index]->getMatrice();

        if(curMat.l != curMat.c)
            QMessageBox::critical(this,"Impossible de calculer le determinant","Cette matrice n'est pas carre");
        else
        QMessageBox::information
            (this,"Trace",
             "Trace: " +
             QString::number(Tr(lsWCentral[index]->getMatrice()))
             );
    }
}

void FenetrePrincipale::tabWidgetVide()
{
    if(tabWidget->count() == 0){
        menuMatrice->actions()[1]->setDisabled(true);
        menuMatrice->actions()[2]->setDisabled(true);
        menuMatrice->actions()[5]->setDisabled(true);
        volet->setEnabled(false);
        menuCalcul->setEnabled(false);
    }
    else{
        menuCalcul->setEnabled(true);
        volet->setEnabled(true);
        menuMatrice->actions()[1]->setEnabled(true);
        menuMatrice->actions()[2]->setEnabled(true);
        menuMatrice->actions()[5]->setEnabled(true);
    }
}

void FenetrePrincipale::CalcT()
{

    if(tabWidget->count() != 0){
        int index = tabWidget->currentIndex();
        matrice curMat = lsWCentral[index]->getMatrice();

        CentreFenetre *curWidget = lsWCentral[index];
        matrice t = T(curMat);

        CentreFenetre *newMat = new CentreFenetre(t);
        lsWCentral << newMat ;
        tabWidget->addTab(newMat,"T("+curWidget->getNom()+")");
        newMat->setNom("T("+curWidget->getNom()+")");
    }
}

void FenetrePrincipale::CalcCom()
{
    if(tabWidget->count() != 0){
        int index = tabWidget->currentIndex();
        matrice curMat = lsWCentral[index]->getMatrice();

        if(curMat.c != curMat.l)
            QMessageBox::critical(this,"Impossible de calculer la comatrice","Cette matrice n'est pas carre.");
        else if(curMat.c == 1)
            QMessageBox::critical(this,"Erreur","Impossible de calculer la comatrice de cette matrice");
        else{
            CentreFenetre *curWidget = lsWCentral[index];
            matrice com = Com(curMat);

            CentreFenetre *newMat = new CentreFenetre(com);
            lsWCentral << newMat ;
            tabWidget->addTab(newMat,"Com("+curWidget->getNom()+")");
            newMat->setNom("Com("+curWidget->getNom()+")");
        }
    }
}

void FenetrePrincipale::CalcInvers()
{
    if(tabWidget->count() != 0){

        int index = tabWidget->currentIndex();
        matrice curMat = lsWCentral[index]->getMatrice();

        if(curMat.l != curMat.c)
            QMessageBox::critical(this,"Impossible de calculer l'inverse","Cette matrice n'est pas carré");
        else if(curMat.l==1)
            QMessageBox::critical(this,"Erreur","Impossible de calculer l'inverse de cette matrice");
        else if(Det(curMat) == 0)
            QMessageBox::critical(this,"Impossible de calculer l'inverse"
          ,"Cette matrice est inversible, car son determinant est egal a 0");
        else{
        CentreFenetre *curWidget = lsWCentral[index];
        matrice invrs = Invrs(curMat);

        CentreFenetre *newMat = new CentreFenetre(invrs);
        lsWCentral << newMat ;
        tabWidget->addTab(newMat,"("+curWidget->getNom()+")^(-1)");
        newMat->setNom("("+curWidget->getNom()+")^(-1)");
        }
    }
}

void FenetrePrincipale::RenomMat()
{if(tabWidget->count() != 0){

    QString nomEntree = QInputDialog::getText(
                this,
                "Renommer",
                "Entrez un nouveau nom pour la matrice");
    if(nomEntree!=""){
        //nomm de la matrice
        for(int i=0; i<lsWCentral.size(); i++)
            if(lsWCentral[i]->getNom() == nomEntree){ //si le nom existe deja
                i=0;
                nomEntree += "*";
            }

        lsWCentral[tabWidget->currentIndex()]->setNom(nomEntree);
        tabWidget->setTabText(tabWidget->currentIndex(), nomEntree);
    }
  }
}

void FenetrePrincipale::CalcAdd()
{if(tabWidget->count() != 0){

    QStringList lsMat;
    for(int i=0; i<tabWidget->count(); i++)
        lsMat << lsWCentral[i]->getNom();

    bool ok = false;
    QString matChoisie = QInputDialog::getItem
            (this,
             "Addition",
             "Choisisez la matrice avec laquelle la matrice "
             +lsWCentral[tabWidget->currentIndex()]->getNom()
             +" doit etre additionee.",
             lsMat,0,false,&ok);
    int i=0;
    if(matChoisie != "" && ok){
        for(i=0; i<tabWidget->count(); i++)
            if( lsWCentral[i]->getNom() == matChoisie )
                break;

    matrice curMat = lsWCentral[tabWidget->currentIndex()]->getMatrice();
    matrice addMat = lsWCentral[i]->getMatrice();

    if((curMat.l != addMat.l) || (curMat.c != addMat.c))
        QMessageBox::critical
                (this
                 ,"Erreur"
                 ,"Impossible de calculer la somme de ces deux matrices,"
                 "car elles n'ont pas les memes dimensions");
    else{
        CentreFenetre *tabSom = new CentreFenetre(Plus(curMat,addMat));
        lsWCentral << tabSom ;
        tabWidget->addTab(tabSom,lsWCentral[tabWidget->currentIndex()]->getNom()+" + "+lsWCentral[i]->getNom());
        tabSom->setNom(lsWCentral[tabWidget->currentIndex()]->getNom()+" + "+lsWCentral[i]->getNom());
    }
   }
 }
}

void FenetrePrincipale::CalcSstr()
{if(tabWidget->count() != 0){

    QStringList lsMat;
    for(int i=0; i<tabWidget->count(); i++)
        lsMat << lsWCentral[i]->getNom();

    bool ok = false;
    QString matChoisie = QInputDialog::getItem
            (this,
             "Soustraction",
             "Choisisez la matrice qui doit etre soutraite de "
             +lsWCentral[tabWidget->currentIndex()]->getNom(),
             lsMat,0,false,&ok);
    int i=0;
    if(matChoisie != "" && ok){
        for(i=0; i<tabWidget->count(); i++)
            if( lsWCentral[i]->getNom() == matChoisie )
                break;

    matrice curMat = lsWCentral[tabWidget->currentIndex()]->getMatrice();
    matrice addMat = lsWCentral[i]->getMatrice();

    if((curMat.l != addMat.l) || (curMat.c != addMat.c))
        QMessageBox::critical
                (this
                 ,"Erreur"
                 ,"Impossible de calculer la soustraction de ces deux matrices,"
                 "car elles n'ont pas les memes dimensions");
    else{
        CentreFenetre *tabSom = new CentreFenetre(Moins(curMat,addMat));
        lsWCentral << tabSom ;
        tabWidget->addTab(tabSom,lsWCentral[tabWidget->currentIndex()]->getNom()+" - "+lsWCentral[i]->getNom());
        tabSom->setNom(lsWCentral[tabWidget->currentIndex()]->getNom()+" - "+lsWCentral[i]->getNom());
    }
    }
  }
}

void FenetrePrincipale::CalcPrd()
{if(tabWidget->count() != 0){
    QStringList lsMat;
    for(int i=0; i<tabWidget->count(); i++)
        lsMat << lsWCentral[i]->getNom();
    bool ok = false;
    QString matChoisie = QInputDialog::getItem
            (this,
             "Produit",
             "Choisisez la matrice avec laquelle "
             +lsWCentral[tabWidget->currentIndex()]->getNom()
             +" doit etre multipliee.",
             lsMat,0,false,&ok);
    if(matChoisie  != "" && ok){
        int i=0;
        if(matChoisie != "")
            for(i=0; i<tabWidget->count(); i++)
               if( lsWCentral[i]->getNom() == matChoisie )
                    break;

        matrice curMat = lsWCentral[tabWidget->currentIndex()]->getMatrice();
        matrice addMat = lsWCentral[i]->getMatrice();

        if((curMat.c != addMat.l))
                QMessageBox::critical
                        (this
                       ,"Erreur"
                        ,"Impossible de calculer le produit de ces deux matrices,"
                         "car le nombre de colonnes de la premiere n'est pas"
                         " egal au nombre de ligne de la deuxieme. ");
        else{

            CentreFenetre *tabSom = new CentreFenetre(Prd(curMat,addMat));
            lsWCentral << tabSom ;
            tabWidget->addTab(tabSom,lsWCentral[tabWidget->currentIndex()]->getNom()+" x "+lsWCentral[i]->getNom());
            tabSom->setNom(lsWCentral[tabWidget->currentIndex()]->getNom()+" x "+lsWCentral[i]->getNom());
        }
    }
    }
}

void FenetrePrincipale::Enregistrer()
{
    QString chemin = QFileDialog::getSaveFileName(this,"Enregistrer");
    FILE *fichier = fopen(chemin.toAscii().data(), "w+");
    if(fichier == NULL)
        QMessageBox::critical(this,"Erreur","Impossible d'ouvrir le fichier ");
    else{
        EcrireMat(lsWCentral[tabWidget->currentIndex()]->getMatrice(),fichier);
        fclose(fichier);
    }
}

void FenetrePrincipale::Ouvrir()
{
    QString chemin = QFileDialog::getOpenFileName(this,"Ouvrir un fichier matrice");
    FILE *fichier = fopen(chemin.toAscii().data(),"r");
    if(fichier ==  NULL)
        QMessageBox::critical(this,"Erreur","Impossible d'ouvrir le fichier "+chemin);
    else{

    if(welcome){
        tabWidget->clear();
        welcome = false;
    }

    //lire le fichier:
    int nbrLignes, nbrCln;
    fscanf(fichier,"%d",&nbrLignes); // lire le nombre de lignes
    fscanf(fichier,"%d",&nbrCln); // lire le nombre de colonnes

    matrice mx = newMat(nbrLignes,nbrCln);

    char l[1000];

    for(int i=0; fscanf(fichier,"%s",l) != EOF; i++)
        LireLigne(mx.mat[i], nbrCln, l);

    CentreFenetre *newMatInput = new CentreFenetre(mx);
    lsWCentral << newMatInput ;

    //nomm de la matrice
    QString inputNom = chemin ;
    for(int i=0; i<lsWCentral.size(); i++)
        if(lsWCentral[i]->getNom() == inputNom){ //si le nom existe deja
            i=0;
            inputNom += "*";
        }

    //mettre le nom:
    newMatInput->setNom(inputNom);
    tabWidget->addTab(newMatInput,inputNom);
    }
}
