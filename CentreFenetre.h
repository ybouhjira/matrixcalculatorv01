#ifndef VOLET_H
#define VOLET_H

#include <QTableWidget>
#include "matrice.h"

class CentreFenetre : public QTableWidget
{
    Q_OBJECT
public:
    CentreFenetre(int lgn, int cln);
    CentreFenetre(matrice mat);
    ~CentreFenetre();

    matrice getMatrice();
    QString getNom();
    void setNom(QString);

public slots:
    void LireValeur(int,int);

private:
    matrice *m;
    QString nom;
};

#endif // VOLET_H
