#ifndef MATRICE_H
#define MATRICE_H
#include <cstdio>

//le type matrice:
struct matrice
{
    int l;
    int c;
    double **mat;
};

//cree une nouvelle matrice:
matrice newMat(int l, int c);

//ecrit la latrice dans le fichier:
void EcrireMat(matrice,FILE*);

void LireLigne(double ligne[], int taille, char *l);

#endif // MATRICE_H
