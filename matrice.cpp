#include <cstdlib>
#include <cstdio>
#include "matrice.h"
#include <cstring>
#include <cctype>

// cree une nouvelle matrice
matrice newMat(int l1, int c1)
{
    int i;
    matrice m;
    m.l = l1;
    m.c = c1;
    //allocation dynamique d'un tableau 2 dimensions:
    m.mat = (double**) calloc(l1,sizeof(double*));
    for (i=0;i<l1;i++)
        m.mat[i] = (double*) calloc(c1,sizeof(double));

    return m;
}

void EcrireMat(matrice m, FILE *fichier)
{
    fprintf(fichier,"%d\n",m.l);
    fprintf(fichier,"%d\n",m.c);
    for(int i=0; i<m.l; i++){
        for(int j=0; j<m.c; j++)
            fprintf(fichier,"%lf/",m.mat[i][j]);
        fprintf(fichier,"\n");
    }
}

void LireLigne(double ligne[], int taille, char *l)
{
    char nbr[100];

    //indices:
    int i=0; //case de l[]
    int j=0; //chiffre
    int n=0; //nombre

    while(l[i]!='\0')
    {
        while(isdigit(l[i]) || l[i]=='.'){
            nbr[j]=l[i];
            i++, j++;
        }
        ligne[n] = atof(nbr);
        j=0, i++, n++;
        memcpy(nbr,"",100); //vider la chaine
    }
}
