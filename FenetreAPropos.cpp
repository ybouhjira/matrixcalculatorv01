#include "FenetreAPropos.h"

FenetreAPropos::FenetreAPropos() : QMessageBox()
{
        setIconPixmap( QPixmap(":/img/res/logo.jpg") );
        setWindowTitle("A propos");
        setText(
      " <b>Matrix calculator : </b>                              "
      " <br>Ce programme permet de calculer la somme, la soustraction, la multiplication, "
      "le determinant, la trace, la transpose, la comatrice, et l'inverse des matrices.</br>"
      " <br>_____________________________________________________</br>"
      " <br> <b>Youssef Bouhjira </br>"
      " <br>Mohammed Doulfakar</b></font>"
      " <br>Genie Informatique 1                                 </br>"
      " <br>EST Essaouira - 2011/2012                           </br>");
}
