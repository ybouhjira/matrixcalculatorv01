#include "CentreFenetre.h"
#include "calcul.h"
#include <QMessageBox>
#include <QLineEdit>
#include <QApplication>
#include <cstdio>

CentreFenetre::CentreFenetre(int lgn, int cln) : QTableWidget()
{
    setRowCount(lgn);       //nombre de lignes
    setColumnCount(cln);    //nombre de colnnes

    for(int i=0; i<lgn; i++)
        for(int j=0; j<cln; j++)
            setItem(i,j,new QTableWidgetItem);

    //la matrice:
    m = new matrice;
    m->c = cln;
    m->l = lgn;
    m->mat = (double**) calloc(lgn,sizeof(double));
    for(int i=0; i<lgn; i++)
        m->mat[i] = (double*) calloc(cln, sizeof(double));

    //connections
    connect(this, SIGNAL(cellChanged(int,int)), this ,SLOT(LireValeur(int,int)));
}

CentreFenetre::CentreFenetre(matrice mat) : QTableWidget()
{
    setRowCount(mat.l);       //nombre de lignes
    setColumnCount(mat.c);    //nombre de colnnes

    for(int i=0; i<mat.l; i++)
        for(int j=0; j<mat.c; j++)
            setItem(i,j,new QTableWidgetItem);

    //la matrice:
    m = new matrice;
    m->c = mat.c;
    m->l = mat.l;
    m->mat = (double**) calloc(mat.l,sizeof(double));
    for(int i=0; i<mat.l; i++)
        m->mat[i] = (double*) calloc(mat.c, sizeof(double));


    for(int i=0; i<m->l; i++)
        for(int j=0; j<m->c; j++)
            m->mat[i][j] = mat.mat[i][j] ;

    for(int i=0; i<m->l; i++)
        for(int j=0; j<m->c; j++)
            item(i,j)->setText(QString::number(m->mat[i][j]));
    //connections
    connect(this, SIGNAL(cellChanged(int,int)), this ,SLOT(LireValeur(int,int)));


}

CentreFenetre::~CentreFenetre(){
    free(m->mat);
    free(m);
}

void CentreFenetre::LireValeur(int lgn, int cln)
{
    QString nbr = item(lgn,cln)->text();
    m->mat[lgn][cln] = nbr.toDouble() ;
}

matrice CentreFenetre::getMatrice()
{
    return *m;
}

QString CentreFenetre::getNom()
{
    return nom;
}

void CentreFenetre::setNom(QString txt)
{
    nom = txt;
}
