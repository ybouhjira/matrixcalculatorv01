#include "DimDialog.h"

DimDialog::DimDialog(QWidget *parent) : QWidget(parent)
{
    layout = new QFormLayout;
    spinBoxL = new QSpinBox;
    spinBoxC = new QSpinBox;
    boutonOk = new QPushButton("Ok");
    lineEdit = new QLineEdit;

    setLayout(layout);
    layout->addRow("Nom: ",lineEdit);
    layout->addRow("Nombre de lignes: ",spinBoxL);
    layout->addRow("Nombre de Colonnes: ",spinBoxC);
    layout->addRow("  ",boutonOk);

    spinBoxL->setMinimum(1);
    spinBoxL->setMaximum(100);
    spinBoxC->setMinimum(1);
    spinBoxC->setMaximum(100);

    layout->setSizeConstraint(QLayout::SetFixedSize);

    connect(boutonOk, SIGNAL(clicked()) ,this ,SLOT(SaveLC()));
}

void DimDialog::SaveLC()
{
    close();
    l = spinBoxL->value();
    c = spinBoxC->value();
    nom = lineEdit->text();
    emit pushedOk();
}

int DimDialog::getC(){
    return c;
}

int DimDialog::getL(){
    return l;
}

QString DimDialog::getNom(){
    return nom;
}
