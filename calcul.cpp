#include <cstdlib>
#include <cmath>
#include <cstdio>
#include "calcul.h"
#include "matrice.h"
#include <QMessageBox>

//donne la sous matrice pour calculer le determinant:
matrice Smat(matrice mat,int x, int y)
{
    int i,j,i1,j1;
    matrice smat = newMat(mat.l-1,mat.l-1);

    for(i=i1=0; i<mat.c; i++){
        for(j=0,j1=0; j<mat.c; j++){
            if(j!=x && i!=y)
                smat.mat[i1][j1] = mat.mat[i][j];
            if(j!=x)
                j1++;
        }
        if(i!=y)
            i1++;
    }

    return smat;
}

double Det(matrice m)
{
    if(m.c==2)
        return m.mat[0][0]*m.mat[1][1] - m.mat[0][1]*m.mat[1][0];
    else{
        int i;
        double det=0;
        matrice smat;
        for(i=0;i<m.c;i++){
            smat = Smat(m,i,0);
            det += pow(-1,i) * m.mat[0][i] * Det(smat) ;
            free(smat.mat);
        }
        return det;
    }
}

matrice Com(matrice m)
{
    matrice com = newMat(m.l,m.c);
    if(m.l==2){
        com.mat[0][0] =  m.mat[1][1];
        com.mat[0][1] = -m.mat[1][0];
        com.mat[1][0] = -m.mat[0][1];
        com.mat[1][1] =  m.mat[0][0];
    }
    else{
        int i,j;
        matrice smat;
        for(i=0;i<m.l;i++)
            for(j=0;j<m.l;j++){
                smat = Smat(m,j,i);
               com.mat[i][j] = pow(-1,i+j) * Det(smat) ;
               free(smat.mat);
            }
    }
    return com;
}

matrice T(matrice m)
{
    int i,j;
        matrice t = newMat(m.c,m.l);

        for(i=0;i<t.l;i++)
            for(j=0;j<t.c;j++)
                t.mat[i][j] = m.mat[j][i] ;

            return t;
}

matrice Invrs(matrice m)
{
    int i,j;
    matrice invrs = newMat(m.l,m.c) ;
    matrice tCom = T(Com(m));
    double det = Det(m);

    for(i=0;i<m.l;i++)
        for(j=0;j<m.c;j++)
            invrs.mat[i][j] =  tCom.mat[i][j] / det ;
    return invrs;
}

double Tr(matrice m)
{
    double tr=0;
    int i;
    for(i=0;i<m.l;i++)
       tr += m.mat[i][i];
    return tr;
}

matrice Plus(matrice m1, matrice m2)
{
    int i,j;
    matrice som = newMat( m1.l, m1.c );
    for(i=0; i<m1.l; i++)
       for(j=0; j<m1.c; j++)
             som.mat[i][j] = m1.mat[i][j] + m2.mat[i][j] ;
    return som;
}

matrice Moins(matrice m1, matrice m2)
{
    int i,j;
    matrice som = newMat( m1.l, m1.c );
    for(i=0; i<m1.l; i++)
       for(j=0; j<m1.c; j++)
             som.mat[i][j] = m1.mat[i][j] - m2.mat[i][j] ;
    return som;
}

matrice Prd(matrice mat1,matrice mat2)
{
    int i,j,x;
    //allocation dynamique:
    matrice prd = newMat(mat1.l,mat2.c);

    //calcul
    for(i=0; i<mat1.l; i++)
        for(j=0; j<mat2.c; j++)
            for(x=0;x<mat1.c;x++)
                prd.mat[i][j] += mat1.mat[i][x]*mat2.mat[x][j];

    return prd;
}
