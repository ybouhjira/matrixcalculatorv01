#include <QtGui/QApplication>
#include "FenetrePrincipale.h"
#include "CentreFenetre.h"
#include "DimDialog.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    FenetrePrincipale w;
    w.show();

    return app.exec();
}
