#ifndef DIMDIALOG_H
#define DIMDIALOG_H

#include <QWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QFormLayout>
#include <QLineEdit>

class DimDialog : public QWidget
{
    Q_OBJECT
public:
    explicit DimDialog(QWidget *parent = 0);

signals:

public slots:
    void SaveLC();

    int getL();
    int getC();
    QString getNom();
signals:
    void pushedOk();
private:
    QFormLayout *layout;
    QPushButton *boutonOk;
    QSpinBox
        *spinBoxL,
        *spinBoxC;
    QLineEdit *lineEdit;
    int l,c;
    QString nom;


};

#endif // DIMDIALOG_H
