#ifndef CALCUL_H
#define CALCUL_H
#include "matrice.h"
/*donne la sous matrice pour le calcul
du determinant et de la comatrice */
matrice Smat(matrice,int,int);

//calcul le determinant
double Det(matrice);

//calcul de comatrice
matrice Com(matrice);

//calcul de la transposé
matrice T(matrice m);

//calcul de l'inverse
matrice Invrs(matrice m);

//calcul de la trace
double Tr(matrice m);

//calcul de la somme:
matrice Plus(matrice, matrice);

//calcul de la soustraction:
matrice Moins(matrice,matrice);

//calcul du produit:
matrice Prd(matrice,matrice);

#endif // CALCUL_H
