#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QList>
#include <QMdiArea>
#include <QToolBar>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QDockWidget>
#include <QTableWidget>
#include <QLabel>
#include "FenetreAPropos.h"
#include "CentreFenetre.h"
#include "DimDialog.h"

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = 0);
private:
    inline void Setup();
    inline void Connections();

public slots:
    void tabWidgetVide();
    void AjouterMat();
    void SuprrMat();
    void RenomMat();

    void CalcDet();
    void CalcTr();
    void CalcT();
    void CalcCom();
    void CalcInvers();

    void CalcAdd();
    void CalcSstr();
    void CalcPrd();

    void Enregistrer();
    void Ouvrir();
private:
    bool welcome;
    QMenu
        *menuMatrice,
        *menuAide,
        *menuCalcul;
    QToolBar *toolBarMat;
    //QMdiArea *mdiArea;
    QTabWidget *tabWidget;
    FenetreAPropos about;
    DimDialog dialog;
    QList<CentreFenetre*> lsWCentral;

    //volet de control:
    QHBoxLayout *layout;
    QDockWidget *volet;
    QPushButton
        *boutonAdd,
        *boutonSstr,
        *boutonPrd,
        *boutonT,
        *boutonTr,
        *boutonCom,
        *boutonInv,
        *boutonDet;
    QScrollArea *scrollArea;
    QWidget *zoneBoutons;
    QLabel *labelBienvenue;
    QPixmap *imagePng;
};

#endif // FENETREPRINCIPALE_H
